from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView

from .views import HelloWorlsView
from .views import SitemapXmlView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', HelloWorlsView.as_view(), name='home'),
    # url(r'^hello$', HelloWorlsView.as_view(), name='home'),
    # url(r'^sitemap\.xml$', SitemapXmlView.as_view(), name='sitemap'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', 'posts.views.main', name='main'),
    url(r'^post/(\d+)$', 'posts.views.showpost', name='showpost'),
    url(r'^post/(\d+)/edit$', 'posts.views.createpost', name='createpost'),
    url(r'^post/(\d+)/delete$', 'posts.views.deletepost', name='deletepost'),
    url(r'^createpost$', 'posts.views.createpost', name='createpost'),
    url(r'^posting$', 'posts.views.posting', name='posting'),

    url(r'^admin/', include(admin.site.urls)),
)
