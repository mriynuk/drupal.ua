from django.views.generic.base import TemplateView
from datetime import date

class HelloWorlsView(TemplateView):
	"""docstring for ClassName"""
	template_name = "helloworld.html"

	def get_context_data(self, **kwargs):
		return {'date': date.today()}

class SitemapXmlView(TemplateView):
	"""docstring for ClassName"""
	template_name = "sitemap.xml"