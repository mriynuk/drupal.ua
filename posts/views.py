from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.core.exceptions import PermissionDenied
from django.shortcuts import render_to_response
from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth.models import User

from posts.models import Posts, PostForm, Comments, CommentForm

def main(request):
    """Main listing."""
    posts = Posts.objects.all().order_by("-created")
    paginator = Paginator(posts, 10)

    try:
        page = int(request.GET.get("page", '1'))
    except ValueError:
        page = 1

    try:
        posts = paginator.page(page)
    except (InvalidPage, EmptyPage):
        posts = paginator.page(paginator.num_pages)

    return render_to_response("list.html", dict(posts=posts, user=request.user))

def showpost(request, pk):
    """Single post with comments and a comment form."""
    post = get_object_or_404(Posts, pk=int(pk))
    comments = Comments.objects.filter(post_id=post.id).order_by("-created")

    form = CommentForm(request.POST or None)

    if request.method == 'POST':
        form = CommentForm(request.user, request.POST)
    else:
        form = CommentForm(request.GET)
    if form.is_valid():
        submission = form.save(commit=False)
        submission.autor_id = request.user.id
        submission.post_id = post.id
        submission.save()
    else:
        form = form

    return render_to_response("post.html", dict(post=post, comments=comments, user=request.user, form=form))

def createpost(request, id=False):
    """Form to create new post."""
    if id:
        post = get_object_or_404(Posts, id=id)
        if request.user.id != post.autor_id and not request.user.is_staff:
            raise PermissionDenied
        form = PostForm(request.POST or None, instance=post)
    else:
        form = PostForm(request.POST)
    return render_to_response("createpost.html", dict(user=request.user, form=form))

def posting(request):
    """Form to create new post."""
    form = PostForm()
    if request.method == 'POST':
        form = PostForm(request.user, request.POST)
    else:
        form = PostForm(request.GET)
    if form.is_valid():
        submission = form.save(commit=False)
        submission.autor_id = request.user.id
        submission.save()
    else:
        form = form

    return redirect('/')

def deletepost(request, id):
    """Form to create new post."""
    post = get_object_or_404(Posts, id=id)
    if request.user.id != post.autor_id and not request.user.is_staff:
        raise PermissionDenied
    post.delete()
    return redirect('/')
